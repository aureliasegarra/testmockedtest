FROM amazoncorretto:18

COPY target/*.jar /opt/app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/opt/app.jat"]