import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/contacts")
public class ContactController {

    private final ContactService contactService = new ContactService();

    @GetMapping
    public List<String> listAll() throws Exception{
        return contactService.getContact();
    }

}
