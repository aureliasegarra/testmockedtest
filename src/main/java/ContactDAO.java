import java.util.ArrayList;
import java.util.List;

public class ContactDAO {
    private List<String> emails = new ArrayList<>();
    public void add(String email) throws DuplicateDataException {
        if(emails.contains(email)){
            throw  new DuplicateDataException("email existant" + email);
        }
        emails.add(email);
    }
    public List<String> getEmails() {
        return emails;
    }

    @Override
    public String toString() {
        return "ContactDAO{" +
                "emails=" + emails +
                '}';
    }
}
