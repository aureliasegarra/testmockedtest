public class DuplicateDataException extends Throwable {
    public DuplicateDataException(String e) {
        super(e);
    }
}
