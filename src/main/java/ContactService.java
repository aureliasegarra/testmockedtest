import java.util.List;

public class ContactService {
    private EmailValidator emailValidator = new EmailValidator();
    private ContactDAO contactDAO = new ContactDAO();

    public void addContact(String email) {
        if (emailValidator.isValid(email)){
            try {
                contactDAO.add(email);
            } catch (DuplicateDataException e) {
                throw new RuntimeException("Mail deja present");
            }
        } else {
            throw new RuntimeException("Mail invalide");
        }
    }
    public ContactDAO getContactDAO() {
        return contactDAO;
    }

    public List<String> getContact(){
        return contactDAO.getEmails();
    }
}
