import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestEmailValidator {
    ContactService contactService = new ContactService();

    @Test
    public void shouldInsertValidEmail() {
        contactService.addContact("Test@gmail.com");
    }

    @Test
    public void shouldRefuseInvalidEmail(){
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("T@gmail.com"));
    }

    @Test
    public void shouldRefuseDoubleEmail() {
        contactService.addContact("try@gmail.com");
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("try@gmail.com"));
    }
}