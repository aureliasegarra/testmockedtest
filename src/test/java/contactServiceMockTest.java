import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
public class contactServiceMockTest {

    @InjectMocks
    private ContactService service = new ContactService();

    @Mock
    private ContactDAO dao;

    void shouldTest() throws DuplicateDataException{
        Mockito.doThrow(new DuplicateDataException("error"))
            .when(dao)
            .add(Mockito.eq("test@gmail.com"));

        Assertions.assertThrows(RuntimeException.class, () -> service.addContact("test@gmail.com"));

    }

}
